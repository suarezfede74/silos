import React from 'react';
import './loader.css';

const Loader = () => {
    return (
        <div class="loading">
            <div class="dot"></div>
            <div class="dot"></div>
            <div class="dot"></div>
            <div class="dot"></div>
            <div class="dot"></div>
        </div>
    )
}

export default Loader
