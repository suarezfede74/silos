import React from 'react';
import './spinners.css'

const Fan = (props) => {
    return (
        <div className="fanContainer">
            <div id={props.spin ? "fan" : "stoppedFan"}></div>
        </div>
    )
}

export default Fan
