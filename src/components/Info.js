import React, {useState, useEffect} from 'react';
import Term from '../assets/term.png';
import {getTempHum} from '../services/ClimaService';
import './info.css'

const Info = () => {
    const [infoClass, setClass] = useState('expanded');
    const [date, setDate] = useState('');
    const [temp, setTemp] = useState('');
    const [hum, setHum] = useState('');

    let userHasScrolled = false;
    window.onscroll = function (e)
    {
        userHasScrolled = true;
    }
    setInterval(() =>{
        if(userHasScrolled && window.screen.width < 540){
            setTimeout(() =>{
                setClass('closed');
                userHasScrolled = false;
            }, 500);
        }
    } , 1000 )

    const updateData = async () => {
        var auxDate = new Date();
        var d = String(auxDate.getDate()).padStart(2, '0');
        var m = String(auxDate.getMonth() + 1).padStart(2, '0');
        var y = auxDate.getFullYear();
        var h = String(auxDate.getHours());
        var mm = String(auxDate.getMinutes());
        if (mm.length === 1) mm = '0'+mm;
        auxDate = m + '/' + d + '/' + y + ' - ' + h + ':' + mm;  
        setDate(auxDate)
        let response = await getTempHum();
        if(response !== undefined){
            setTemp(response.temperature);
            setHum(response.humidity);
        } 
    }
    useEffect(() => {
        updateData();
        setInterval(updateData, 60000);
    }, []);
    


    return (
        <div className={infoClass} onClick={() => (infoClass === 'closed' || window.screen.width >= 540) ? setClass('expanded') : setClass('closed')}>
            <img src={Term} className="tempIcon" alt="tempIcon"/>
            <div className="info">
                <p className="infoDate">{date}</p>
                <div>Temperatura: {temp}</div>
                <div>Humedad: {hum}</div>
            </div>
        </div>
    )
}

export default Info
