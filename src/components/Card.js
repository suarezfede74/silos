import React, {useState, useEffect, useCallback} from 'react';
import './card.css';
import Silo from '../assets/silos.png';
import Fan from './spinners/Fan'
import Switch from './inputs/Switch'
import { getDetails } from '../services/DetailsService';

const Card = (props) => {
    const [siloState, setSiloState] = useState('initial');
    const [cardClass, setCardClass] = useState('card');
    const [siloData, setSiloData] = useState({
        cod: 0,
        body: {
            hayDatos: true,
        }
    });

    useEffect(() => {
        console.log("siloData: ", siloData)
    }, [siloData])

    const getSiloData = useCallback( async () =>{
        let res = await getDetails(props.id, props.data.aux);
        setSiloData(res);
    }, [props]);

    useEffect(() => {
        console.log("siloData has changed: ", siloData);
    }, [siloData])

    useEffect(() =>{
        getSiloData();
    }, [props, getSiloData])

    useEffect(() => {
        switch(siloState){
            case 'initial':
                setCardClass('card');
                break;
            case 'Limited':
                setCardClass('card greenCard');
                break;
            case 'Forced':
                setCardClass('card greenCard');
                break;
            case 'Error':
                setCardClass('card redCard');
                break;
            default:
                setCardClass('card');
                break;
        }
    }, [siloState]);

    const setSiloHandler = (value) => {
        if (siloState !== value){
            setSiloState(value);
        }
        else{
            setSiloState('initial');
        }
    }

    return (
        <div className={cardClass}>
            <div className="siloID">{props.id}</div>
            <div className="cargImgContainer" onClick={() => props.setId(props.id)}>
                <img src={Silo} alt="silo"></img>
            </div>
            <div className="cardPanel">
                <Fan spin={siloState !== 'initial'}/>
                {
                    !siloData.body.hayDatos ?
                    <div className="checksPanel">
                        <div className="checkContainer" onClick={() => setSiloHandler('Limited')}>
                            <p>Limitado</p>
                            <Switch checked={siloState === 'Limited'}/>
                        </div>
                        <div className="checkContainer" onClick={() => setSiloHandler('Error')}>
                            <p>Forzado</p>
                            <Switch checked={siloState === 'Error'}/>
                        </div>
                    </div> :
                    <div className="panelWithData">
                        <div className="cardDataContainer">
                            <p>Grano: {siloData.body.grano}</p>
                            <p>Temperatura: {siloData.body.temperatura}</p>
                            <p>Humedad: {siloData.body.humedad}</p>
                            <p>Toneladas: {siloData.body.toneladas}</p>
                        </div>
                        <div className="checksPanel">
                            <div className="checkContainer" onClick={() => setSiloHandler('Limited')}>
                                <p>Limitado</p>
                                <Switch checked={siloState === 'Limited'}/>
                            </div>
                            <div className="checkContainer" onClick={() => setSiloHandler('Error')}>
                                <p>Forzado</p>
                                <Switch checked={siloState === 'Error'}/>
                            </div>
                        </div>
                    </div>
                }
            </div>
        </div>
    )
}

export default Card
