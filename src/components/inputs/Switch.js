import React from 'react';
import './switch.css'

const Switch = (props) => {
    return (
        <div className="switch" id="cardSwitch">
            <input type="checkbox" checked={props.checked} onChange={() => 0}/>
            <span></span>
        </div>
    )
}

export default Switch
