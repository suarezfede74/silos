//Las constantes como la url deberían guardarse en un archivo de constantes globales
//const URL = '' complenten esto con la url de la API una vez que esté lista

export const getDetails = async(id, aux) => { //el auxiliar simplemente me va a ayudar a testear el comportamiento cuando no hay datos
    try{
        //const response = await fetch(`${URL}/id=${id}`);
        //const resData = await response.json();
        //supongamos que la respuesta es algo como esto:
        const resData = {
            cod: 200,
            body: {
                aux: true,
                hayDatos: true,
                marcha: 4,
                descanso: 2,
                totalHoras: 40,
                grano: 'maiz',
                toneladas: 2000,
                humedad: 0,
                temperatura: 0
            }
        }
        if(resData.cod === 200 && aux){//cuando se haga un fetch real saquen el && aux
            return resData;
        }
        else{
            return {
                cod: 200,
                body: {
                   hayDatos: false 
                }
            }
        }
    }
    catch(err){
        console.error(err);
    }
}

export const postDetails = async(data, siloData, id, setSiloData) => {
    try{
        //const response = await fetch(`${URL}/id=${id}`); incluir todos los datos de la misma forma que el id
        //const resData = await response.json();
        //supongamos que la respuesta es algo como esto:
        const resData = {
            cod: 200
        }
        if(resData.cod === 200){//handleamos un posible error
            let aux = siloData;
            aux[id].aux = true;
            setSiloData(aux);
            return resData;
        }
    }
    catch(err){
        console.error(err);
    }
}