import React, {useState, useEffect} from 'react';
import Card from "../components/Card";
import Info from '../components/Info'
import Details from './Details'
import './main.css'

const Main = () => {
    const [id, setId] = useState(0);
    const [siloData, setSiloData] = useState({
        "1": {
            aux: false, //es para simular los datos subidos al backend
            hayDatos: false,
            marcha: null,
            descanso: null,
            totalHoras: null,
            grano: '',
            toneladas: null,
            humedad: null,
            temperatura: null
        },
        "2": {
            aux: false, //es para simular los datos subidos al backend
            hayDatos: false,
            marcha: null,
            descanso: null,
            totalHoras: null,
            grano: '',
            toneladas: null,
            humedad: null,
            temperatura: null
        },
        "3": {
            aux: false, //es para simular los datos subidos al backend
            hayDatos: false,
            marcha: null,
            descanso: null,
            totalHoras: null,
            grano: '',
            toneladas: null,
            humedad: null,
            temperatura: null
        },
        "4": {
            aux: false, //es para simular los datos subidos al backend
            hayDatos: false,
            marcha: null,
            descanso: null,
            totalHoras: null,
            grano: '',
            toneladas: null,
            humedad: null,
            temperatura: null
        },
        "5": {
            aux: false, //es para simular los datos subidos al backend
            hayDatos: false,
            marcha: null,
            descanso: null,
            totalHoras: null,
            grano: '',
            toneladas: null,
            humedad: null,
            temperatura: null
        },
        "6": {
            aux: false, //es para simular los datos subidos al backend
            hayDatos: false,
            marcha: null,
            descanso: null,
            totalHoras: null,
            grano: '',
            toneladas: null,
            humedad: null,
            temperatura: null
        },
        "7": {
            aux: false, //es para simular los datos subidos al backend
            hayDatos: false,
            marcha: null,
            descanso: null,
            totalHoras: null,
            grano: '',
            toneladas: null,
            humedad: null,
            temperatura: null
        },
        "8": {
            aux: false, //es para simular los datos subidos al backend
            hayDatos: false,
            marcha: null,
            descanso: null,
            totalHoras: null,
            grano: '',
            toneladas: null,
            humedad: null,
            temperatura: null
        },
        "9": {
            aux: false, //es para simular los datos subidos al backend
            hayDatos: false,
            marcha: null,
            descanso: null,
            totalHoras: null,
            grano: '',
            toneladas: null,
            humedad: null,
            temperatura: null
        },
        "10": {
            aux: false, //es para simular los datos subidos al backend
            hayDatos: false,
            marcha: null,
            descanso: null,
            totalHoras: null,
            grano: '',
            toneladas: null,
            humedad: null,
            temperatura: null
        },
        "11": {
            aux: false, //es para simular los datos subidos al backend
            hayDatos: false,
            marcha: null,
            descanso: null,
            totalHoras: null,
            grano: '',
            toneladas: null,
            humedad: null,
            temperatura: null
        },
        "12": {
            aux: false, //es para simular los datos subidos al backend
            hayDatos: false,
            marcha: null,
            descanso: null,
            totalHoras: null,
            grano: '',
            toneladas: null,
            humedad: null,
            temperatura: null
        },
    })

    const setDataHandler = (value) => {
        setSiloData(value);
        setTimeout(() =>{
            console.log("siloData: ", siloData);
        }, 1000)
    }
    useEffect(() => {
        console.log(id)
    }, [id])
    useEffect(() => {
        console.log("Aux: ", siloData)
    }, [siloData])

    return (
        <div className="Main">
            <Info />
            <div className="cardsContainer">
                <Card setId={setId} id="1" data={siloData["1"]}/>
                <Card setId={setId} id="2" data={siloData["2"]}/>
                <Card setId={setId} id="3" data={siloData["3"]}/>
                <Card setId={setId} id="4" data={siloData["4"]}/>
                <Card setId={setId} id="5" data={siloData["5"]}/>
                <Card setId={setId} id="6" data={siloData["6"]}/>
                <Card setId={setId} id="7" data={siloData["7"]}/>
                <Card setId={setId} id="8" data={siloData["8"]}/>
                <Card setId={setId} id="9" data={siloData["9"]}/>
                <Card setId={setId} id="10" data={siloData["10"]}/>
                <Card setId={setId} id="11" data={siloData["11"]}/>
                <Card setId={setId} id="12" data={siloData["12"]}/>
            </div>
            <Details id={id} setId={setId} siloData={siloData} setSiloData={setDataHandler}/>
        </div>
    )
}

export default Main
