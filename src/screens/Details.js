import React, {useState, useEffect, useCallback} from 'react';
import {getDetails, postDetails} from '../services/DetailsService';
import Loader from '../components/spinners/Loader'
import './details.css';

const Details = (props) => {
    const [details, setDetails] = useState('');
    const [isLoading, setLoading] = useState(true);
    const [data, setData] = useState({
        'humedad': -1,
        'toneladas': -1,
        'temperatura': -300,
        'grano': ''
    });


    const getDetailsFunc = useCallback( async () =>{
        let res = await getDetails(props.id, props.siloData[props.id] ? props.siloData[props.id].aux : false);
        if(res.body.hayDatos){
            let auxData = props.siloData;
            auxData[props.id] = res.body;
            props.setSiloData(auxData)
            setDetails(res.body);
        }
        else{
            setDetails(null);
        }
        setLoading(false);
    }, [props]);

    const postDetailsFunc =  async () => {
        let res = await postDetails(data, props.siloData, props.id, props.setSiloData);
        console.log(res)
        setData({
            'humedad': -1,
            'toneladas': -1,
            'temperatura': -300,
            'grano': ''
        });
        props.setId(0);
    };

    const sendHandle = () =>{
        
        if(data.humedad < 0 || data.toneladas < 0 || data.temperatura < -300 || data.grano === ''){
            alert("Complete todos los campos para continuar.");
        }
        else{
            postDetailsFunc();
        }
    }

    const updateData = (name, value) =>{
        let auxData = data;
        auxData[name] = value;
        setData(auxData)
    }

    useEffect(() => {
        getDetailsFunc();
    }, [getDetailsFunc, props.siloData])

    return (
        <div className={props.id !== 0 ? "detailsContainer" : "detailsContainer detailsClosed"} >
            <div className="closeDetails" onClick={() => props.setId(0)}>X</div>
            <div className="detailsContent">
                <h1>Silo Nro. {props.id}</h1>
                {
                    details !== null ? 
                        isLoading ?
                            <Loader /> : 
                            <div className="detailsPanel">
                                <h1>Información de grano</h1>
                                <div className="detailContainer">
                                    <p>Marcha: {details.marcha}</p>
                                </div>
                                <div className="detailContainer">
                                    <p>Descanso: {details.descanso}</p>
                                </div>
                                <div className="detailContainer">
                                    <p>Total horas: {details.totalHoras}</p>
                                </div>
                                <div className="detailContainer">
                                    <p>Grano: {details.grano}</p>
                                </div>
                                <div className="detailContainer">
                                    <p>Toneladas: {details.toneladas}</p>
                                </div>
                                <div className="detailContainer">
                                    <p>Humedad: {details.humedad}</p>
                                </div>
                                <div className="detailContainer">
                                    <p>Temperatura: {details.temperatura}</p>
                                </div>
                                <div className="detailContainer">
                                    <div>CANCELAR</div>
                                </div>
                            </div> :
                    <div className="detailsForm">
                        <div className="inputContainer">
                            <label>Humedad</label>
                            <input type="number" onChange={(e) => updateData('humedad', e.target.value)}/>
                        </div>
                        <div className="inputContainer">
                            <label>Toneladas</label>
                            <input type="number" onChange={(e) =>  updateData('toneladas', e.target.value)}/>
                        </div>
                        <div className="inputContainer">
                            <label>Temperatura</label>
                            <input type="number" onChange={(e) =>  updateData('temperatura', e.target.value)}/>
                        </div>
                        <div className="inputContainer">
                            <label>Grano</label>
                            <input type="text" onChange={(e) =>  updateData('grano', e.target.value)}/>
                        </div>
                        <div className="inputContainer">
                            <div onClick={sendHandle}>CONFIRMAR</div>
                        </div>
                    </div>
                }
            </div>
        </div>
    )
}

export default Details
